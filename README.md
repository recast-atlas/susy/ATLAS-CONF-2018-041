# MBJ RECAST ATLAS-CONF-2018-041

This runs the MBJ analysis as a workflow:

1. Runs the Event Selection for MC16a and MC16d samples
2. Weights the selected events according to the cross section supplied
   by in the MBJ JSON format (see examples/inputdata)
3. Runs single-bin exclusion fit. The data and background
   fit inputs are supplied to as a HistFitter cache (see examples/inputdata)

<img src="examples/viz.png"  width="750">

## Required Input

The inputs is to be provided as in examples/test_case_375893.yml with input files
as in examples/inputdata. The Derivation files must be in DAOD_SUSY10 format
and can be provided as a SampleHandler compatible XrootD directory path.

```
> ls examples/inputdata 
cache.root  mc16a_weights.json  mc16d_weights.json
> tree /eos/project/r/recast/atlas/ATLAS-CONF-2018-041/testdata/signal_evsel_inputs
/eos/project/r/recast/atlas/ATLAS-CONF-2018-041/testdata/signal_evsel_inputs
├── mc16_13TeV.375893.MGPy8EG_A14N_GG_ttn1_2100_5000_1.deriv.DAOD_SUSY10.e6351_e5984_a875_r10201_r10210_p3401
│   └── DAOD_SUSY10.13429112._000001.pool.root.1
└── mc16_13TeV.375893.MGPy8EG_A14N_GG_ttn1_2100_5000_1.deriv.DAOD_SUSY10.e6351_e5984_a875_r9364_r9315_p3387
    └── DAOD_SUSY10.13001376._000001.pool.root.1
```

The data is passed to the workflow as a YAML or JSON file:

```
> cat examples/test_case_375893.yml
databkgcache: 'cache.root'
weightfiles: [mc16a_weights.json,mc16d_weights.json]
did: 375893
inputdata_xrootd: root://eosuser.cern.ch//eos/project/r/recast/atlas/ATLAS-CONF-2018-041/testdata/signal_evsel_inputs
mc16a_pattern: r9364
mc16d_pattern: r10201
```

## Running the Reinterpretation

Execute the workflow by specifying a work directory, the workflow and input data:
```
yadage-run \
work \
workflow.yml -t gitlab-cern:recast-atlas/susy/ATLAS-CONF-2018-041:specs \
-d initdir=$PWD/examples/inputdata examples/test_case_375893.yml
```

## Results:

For each region, we obtain a HistFitter JSON document with the fit results for
the one point that was processed. The files are stored in the "runfit/result_json"
directory of the workflow.

```
> yadage-run work workflow.yml -t specs -d initdir=$PWD/examples/inputdata examples/test_case_375893.yml

> echo "  Region | CLs";ls work/runfit/result_json/*.json|while read x;do echo "$(echo $x|awk -F_ '{printf "%4s %4s|", $8,$9}') $(jq .[].CLs < $x)";done
  Region | CLs
 Gbb    B| 0.0801708
 Gbb    C| 0.4327427
 Gbb    M| 0.4009449
 Gbb   VC| 0.8292654
 Gtt   0L| 0.2552493
 Gtt   0L| 0.8917018
 Gtt   0L| 0.3842967
 Gtt   1L| 0.004965814
 Gtt   1L| 0.1869514
 Gtt   1L| 0.005948481
```

## Software

The analysis runs based on two docker images built from the following repos:

1. Event Selection: 
   - Repo: https://gitlab.cern.ch/MultiBJets/MBJ_Analysis
   - Image: gitlab-registry.cern.ch/recast-atlas/susy/atlas-conf-2018-041/mbj_analysis:ATLAS-CONF-2018-041

2. Statistical Analysis: 
   - Repo: https://gitlab.cern.ch/MultiBJets/MBJ_HistFitter
   - Image: gitlab-registry.cern.ch/recast-atlas/susy/atlas-conf-2018-041/mbj_histfitter:ATLAS-CONF-2018-041


